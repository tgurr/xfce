# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]
require gsettings
require github [ user='the-cavalry' tag="v${PV}" ]

SUMMARY="A simple locker that integrates with lightdm"
SLOT="0"

MYOPTIONS="
    consolekit
    gtk2
    systemd
    upower
"

PLATFORMS="~amd64"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.35.0]
        sys-devel/gettext
        xfce-extra/xfce4-dev-tools
    build+run:
        dev-libs/dbus-glib[>=0.30]
        dev-libs/glib[>=2.25.6]
        x11-apps/lightdm[>=1.7.0]
        x11-libs/libXext
        x11-server/xorg-server[>=1.0]
        consolekit? ( sys-auth/ConsoleKit2 )
        gtk2?       ( x11-libs/gtk+:2[>=2.24.0] )
        !gtk2?      ( x11-libs/gtk+:3[>=2.99.3] )
        systemd?    ( sys-apps/systemd )
        upower?     ( sys-apps/upower )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-late-locking
    --with-dpms-ext
    --with-mit-ext
)

src_prepare() {
    default

    autotools_select_versions
    # The package supplies ./autogen.sh which runs xdg-autogen from
    # xfce4-dev-tools and creates configure.ac from configure.ac.in
    edo ./autogen.sh
}

src_configure() {
    opts=(
        ${DEFAULT_SRC_CONFIGURE_PARAMS[@]}
        $(if option systemd || option consolekit;then
            echo --enable-lock-on-suspend
        fi)
        option_with "consolekit console-kit"
        option_with gtk2
        option_with systemd
        option_with upower
    )

    econf "${opts[@]}"
}

